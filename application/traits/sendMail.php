<?php
	trait sendMail
	{
		public function configMail()
		{
			$mailer = new PHPMailer\PHPMailer\PHPMailer();
			$mailer->isSMTP();
			$mailer->SMTPAuth = true;
			$mailer->SMTPSecure = "ssl";
			$mailer->Host = MAIL_HOST;
			$mailer->Port = MAIL_PORT;
			$mailer->Username = MAIL_USER;
			$mailer->Password = MAIL_PASW;
			$mailer->CharSet = 'UTF-8';
			$mailer->SetFrom(MAIL_USER, "InterisJob");
			return $mailer;
		}
		public function sendMail($emailTo, $nameTo, $subject, $body)
		{
			$mailer = $this->configMail();
			
			$mailer->Subject = $subject;
			$mailer->MsgHTML($body);
			$mailer->AddAddress($emailTo, $nameTo);
			
			return $mailer->Send();
		}

		public function sendMailMany($addresses, $subject, $bodyHtml, $bodyText)
		{
			$mailer = $this->configMail();
			
			$mailer->Subject = $subject;
			$mailer->Body = $bodyHtml;
			$mailer->isHTML(true);
			$mailer->AltBody = $bodyText;

			foreach ($addresses as $address) {
				$mailer->AddBCC($address['email'], $address['name']);
			}
			return $mailer->Send();
		}
	}
