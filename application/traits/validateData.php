<?php
	// filtro de datos
	trait validateData
	{
		public function clean($value = "")
		{
			$value = strip_tags(trim($value));
			return $value;
		}
		
		public function getElapsed( $fecha )
		{
			$FechaInicial = ($fecha!=false)? date("YmdHis",strtotime($fecha)) : date("YmdHis");
			
			$anio 		= date("Y")-date("Y",strtotime($FechaInicial));
			$mes 		= date("m")-date("m",strtotime($FechaInicial));
			$dia 		= date("d")-date("d",strtotime($FechaInicial));
			$hora 		= date("H")-date("H",strtotime($FechaInicial));
			$minuto 	= date("i")-date("i",strtotime($FechaInicial));
			$segundo 	= date("s")-date("s",strtotime($FechaInicial));
			
			if($segundo < 0)
			{
				$segundo+=60;
				$minuto-=1;
			}
			if($minuto < 0)
			{
				$minuto+=60;
				$hora-=1;
			}
			if($hora < 0)
			{
				$hora+=24;
				$dia-=1;
			}
			if($dia < 0)
			{
				$dia+=30;
				$mes-=1;
			}
			if($mes < 0)
			{
				$mes+=30;
				$anio-=1;
			}
			
			if($anio>0)
			{
				return ($anio==1) ? "un año": $anio." años";
			}
			else if($mes>0)
			{
				return ($mes==1) ? "un mes": $mes." meses";
			}
			else if($dia>0)
			{
				return ($dia==1) ? "un dias": $dia." dias";
			}
			else if($hora>0)
			{
				return ($hora==1) ? "una hora": $hora." horas";
			}
			else if($minuto>0)
			{
				return ($minuto==1) ? "un minuto": $minuto." minutos";
			}
			else //if($segundo>0)
			{
				return ($segundo==1) ? "un segundo": $segundo." segundos";
			}
		}
	}
