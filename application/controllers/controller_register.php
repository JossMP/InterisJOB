<?php
	class Controller_Register extends Controller
	{
		use sendMail;
		public function __construct()
		{
			parent::__construct();
			if( !isset($_SESSION) )
			{
				session_start();
			}
			$this->professional = new Model_Professional();
			$this->company = new Model_Company();
			$this->usuario = new Model_Usuario();
		}
		function newProfessional( $usuario, $nombre, $apellidos, $email, $clave )
		{
			$persona = $this->professional->putProfessional();
			
			$persona->nombre = $nombre;
			$persona->apellidos = $apellidos;
			
			$persona->save();
			
			$id = $persona->id();
			if( $id )
			{
				$user = $this->usuario->putUsuario();
				$user->user = $usuario;
				$user->password = md5($clave);
				$user->email = $email;
				$user->tipo = "Persona";
				$user->code = md5("Persona" . $id . $usuario);
				$user->idPersona = $id;
				
				$user->save();
				if( $user->id() )
				{
					$this->validateMail( $email, $usuario, md5("Persona" . $id . $usuario) );
					return true;
				}
				$persona->delete();
				return false;
			}
			return false;
		}
		
		public function checkUser($login)
		{
			if( $this->usuario->checkUser($login) >= 1 )
			{
				return true;
			}
			return false;
		}
		
		public function checkEmail($email)
		{
			if( $this->usuario->checkEmail($email) >= 1 )
			{
				return true;
			}
			return false;
		}
		public function validateMail($email, $name, $code)
		{
			$body = 'Valide su cuenta en IterisJob haciendo click en el Siguiente enlace:<br>
			<a href="https://interisjob.dev/register/vaidation/'.$code.'">VALIDAR</a>
			<br>
			o copie este codigo: <strong>'.$code.'</strong><br>
			y visite este link: <a href="https://interisjob.dev/register/vaidation">https://interisjob.dev/register/vaidation</a>
			';
			return $this->sendMail($email, $name, "[InterisJob] Validacion de Registro", $body);
		}
	}
?>
