<?php
	class Controller_Session extends Controller
	{
		public function __construct()
		{
			parent::__construct();
			if( !isset($_SESSION) )
			{
				session_start();
			}
			if( $this->get("csrf") == false )
			{
				$csrf = md5(uniqid(time()));
				$this->put("csrf", $csrf);
			}
			$this->model = new Model_Usuario();
		}
		// inicia session
		public function getLogin($login,$password)
		{
			$user = $this->model->getUserExist($login,$password);
			if( $user )
			{
				$token = md5($user->idUsuario.uniqid());
				$session = array(
					"login"		=> $user->user,
					"id"		=> $user->idUsuario,
					"tipo"		=> $user->tipo,
					"token"		=> $token
				);
				$this->put_login($session);
				$user->token = $token;
				$user->acceso = date("Y-m-d H:i:s");
				$user->save();
				return true;
			}
			return false;
		}
		// Check session actual
		public function isUser()
		{
			if( $this->get("success")!=false && $this->get("login_id")!=false )
			{
				return true;
			}
			return false;
		}
		// Datos Usuario actual
		public function getUser()
		{
			if( $this->get("tipo") == "Persona" )
			{
				return $this->model->getPersona($this->get("id"));
			}
			else
			{
				return $this->model->getEmpresa($this->get("id"));
			}
			return null;
		}
		// add $_SESSION
		public function put($name, $data)
		{
			$_SESSION[$name] = $data;
		}
		// change $_SESSION
		public function set($name, $data)
		{
			$this->put($name, $data);
		}
		// get $_SESSION[?????]
		public function get($name)
		{
			if(isset($_SESSION[$name]))
			{
				return $_SESSION[$name];
			}
			return false;
		}
		// del $_SESSION[????]
		public function remove($name = null)
		{
			if($name!=null)
			{
				unset($_SESSION[$name]);
			}
			else
			{
				$_SESSION = array();
				session_destroy();
				setcookie("crsf","",time()-100);
				setcookie("login_id","",time()-100);
			}
		}
		
		/* Register Login */
		public function id_login()
		{
			$id_login = uniqid("login_");
			$csrf = md5($id_login);
			$expiry = time()+3600;
			$this->put( "success", true );
			$this->put( "login_id", $id_login );
			$this->put( "expiry", $expiry );
			$this->put( "crsf", $csrf );
			setcookie( "crsf", $csrf, $expiry );
			setcookie( "login_id", $id_login, $expiry );
		}
		public function extend_login()
		{
			if( $this->get("success")!=false && $this->get("login_id")!=false && $this->get("expiry")!=false )
			{
				if( $this->get("expiry") < time() )
				{
					$expiry = time()+3600;
					$this->put( "expiry", $expiry );
					setcookie( "crsf", $this->get("crsf"), $expiry );
					setcookie( "login_id", $this->get("login_id"), $expiry, "/" );
				}
			}
		}
		public function put_login($data) // crea session login
		{
			if( is_array($data) )
			{
				$success=false;
				foreach($data as $i=>$v)
				{
					$this->put($i, $v);
					$success=true;
				}
				if($success)
				$this->id_login();
			}
		}
		public function set_login($data) // Modifica session de login
		{
			if( is_array($data) )
			{
				foreach($data as $i=>$v)
				{
					$this->set($i, $v);
				}
				$this->extend_login();
			}
		}
	}
