<?php
	class Controller_Professional extends Controller
	{
		public function __construct()
		{
			parent::__construct();
			if( !isset($_SESSION) )
			{
				session_start();
			}
			$this->model = new Model_Professional();
		}
		public function getPais($id) // idDistrito
		{
			return $this->model->getPais($id);
		}
		public function getListProfessional($pag=0)
		{
			return $this->model->getListProfessional($pag);
		}
		public function getWorkActivity($id)
		{
			return $this->model->getWorkActivity($id);
		}
		public function getProfession($id) //Profesion por ID de professional
		{
			return $this->model->getProfession($id);
		}
		public function getSkills($id) //habilidades por ID de professional
		{
			return $this->model->getSkills($id);
		}
		public function getAchievement($id) //Logros por ID de professional
		{
			return $this->model->getAchievement($id);
		}
		public function getStudies($id) //Estudios por ID de professional
		{
			return $this->model->getStudies($id);
		}
		public function getExperience($id) //Estudios por ID de professional
		{
			return $this->model->getExperience($id);
		}
	}
