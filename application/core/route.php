<?php
	$routing = new \Mini\Mini();
	
	$base  = dirname($_SERVER['PHP_SELF']);
	if(ltrim($base, '/'))
	{
		$_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], strlen($base));
	}
	
	// initial global config
	$routing->respond(function ($request, $response, $service, $app) //use ($routing)
	{
		$service->fn = new Controller_Traits();
		$service->session = new Controller_Session();
		$service->user = $service->session->getUser();
		//$service->notification = new Controller_Notification(); 
		//$service->message = new Controller_Message();
		//$service->task = new Controller_Task();
		$service->layout(APP.'template/layouts/default.php');
	});
	// error page for code
	$routing->onHttpError(function ($code, $routing) {
		switch ($code) {
			case 404:
				header("HTTP/1.0 404 Not Found");
				$service = $routing->service();
				$service->title = '404 Not Found';
				$service->msg_error = 'Page not found';
				$service->render(APP.'template/404.php');
				break;
			case 405:
				$routing->response()->body( 'Method Not Allowed' );
				break;
			default:
				$routing->response()->body( 'Error '. $code );
		}
	});
	
	// Home Page
	$routing->get("/", function($request, $response, $service, $app)
	{
		$service->title = 'InterisJob';
		$service->render(APP.'template/home.php');
	});
	
	// List Employment
	$routing->get("/employment", function($request, $response, $service, $app)
	{
		$service->title = 'Empleos';
		$service->render(APP.'template/profile_list.php');
	});
	
	// List Professional
	$routing->get("/professional", function($request, $response, $service, $app)
	{
		$service->profesionales = new Controller_Professional();
		$service->title = 'Profesionales';
		$service->render(APP.'template/profile_list.php');
	});
	$routing->get("/professional/pag-[i:pag]", function($request, $response, $service, $app)
	{
		$service->profesionales = new Controller_Professional();
		$service->title = 'Profesionales';
		$service->render(APP.'template/profile_list.php');
	});
	$routing->get("/professional/[a:user]", function($request, $response, $service, $app)
	{
		$response->body($request->user." esta seccion aun no esta implementado...");
	});
	// profile 
	$routing->get("/profile",function($request,$response,$service,$app){
		$service->title="Profile NEW";
		$service->render(APP.'template/profile.php');
	});
	
	// List Company
	$routing->get("/company", function($request, $response, $service, $app)
	{
		$service->title = 'Empresas';
		$service->render(APP.'template/company_list.php');
	});
	
	// Login
	$routing->get("/login", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Inicio de Sesion';
		$service->render(APP.'template/login.php');
	});
	$routing->post("/login", function($request, $response, $service, $app)
	{
		if($service->session->getLogin( $request->usuario, $request->clave ))
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Inicio de Sesion';
		$service->msg_error = 'Usuario o clave incorrecta!!';
		$service->render(APP.'template/login.php');
	});
	$routing->get("/logout", function($request, $response, $service, $app)
	{
		$service->session->remove( $request->usuario, $request->clave );
		$response->redirect('/login')->send();
	});
	
	// Register
	$routing->get("/register", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Registro';
		$service->render(APP.'template/register.php');
	});
	$routing->get("/register/job", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Registro de usuario';
		$service->render(APP.'template/register_job.php');
	});
	$routing->post("/register/job", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->registrar = new Controller_Register();
		if( $request->clave != $request->clave2 )
		{
			$service->msg_error = 'La contraseña no coincide!!';
		}
		else if( $service->registrar->checkEmail($request->email) )
		{
			$service->msg_error = 'E-mail ya esta usado por otro usuario!!';
		}
		else if( $service->registrar->checkUser($request->user) )
		{
			$service->msg_error = 'Nombre de usuario ya existe!!';
		}
		else
		{
			if ( $service->registrar->newProfessional($request->user, $request->nombre, $request->apellidos, $request->email, $request->clave ) )
			{
				$response->redirect('/register/validation')->send();
			}
			else
			{
				$service->msg_error = 'ERROR: db no disponible!!';
			}
		}
		$service->title = 'Registro de usuario';
		$service->render(APP.'template/register_job.php');
	});
	
	$routing->get("/register/validation", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Validacion de Usuario';
		$service->render(APP.'template/register_validation.php');
	});
	$routing->post("/register/validation", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Validacion de Usuario';
		$service->render(APP.'template/register_validation.php');
	});
	$routing->get("/register/validation/[a:codeurl]", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->msg_validation_type = 'success';
		$service->msg_validation = 'Su cuenta ha sido activada, ya puede <a href="'.$service->url("/login").'">Iniciar Session</a>.';
		$service->title = 'Validacion de Usuario';
		$service->render(APP.'template/register_validation.php');
	});
	$routing->get("/register/company", function($request, $response, $service, $app)
	{
		if($service->session->isUser())
		{
			$response->redirect('/')->send();
		}
		$service->title = 'Registro de empresas';
		$service->render(APP.'template/register_company.php');
	});
	
	$routing->dispatch();
?>
