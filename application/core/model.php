<?php
	abstract class Model
	{
		protected $table = '';
		public function __construct()
		{
			ORM::configure(array(
				'connection_string' 	=> DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME,
				'username' 				=> DB_USER,
				'password' 				=> DB_PASS
			));
			ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES '.DB_CHARSET));
			
			ORM::configure('id_column_overrides', array(
				'Persona' 			=> 'idPersona',
				'Usuario' 			=> 'idUsuario',
				'Empresa' 			=> 'idEmpresa',
				'Habilidad' 		=> 'idHabilidad',
				'Logro' 			=> 'idLogro',
				'Profesion' 		=> 'idProfesion',
				'Estudio' 			=> 'idEstudio',
				'Experiencia' 		=> 'idExperiencia',
				'Habilidad' 		=> 'idHabilidad',
				'Logro' 			=> 'idLogro',
				'Mensaje' 			=> 'idMensaje',
				'Notificacion' 		=> 'idNotificacion',
				'Tarea' 			=> 'idTarea',
				'ActividadLaboral' 	=> 'idActividadLaboral'
			));
		}
		
		public function beginTransaction()
		{
			return ORM::get_db()->beginTransaction();
		}

		public function commitTransaction()
		{
			return ORM::get_db()->commit();
		}

		public function rollbackTransaction()
		{
			return ORM::get_db()->rollBack();
		}

		public function get($id)
		{
			return ORM::for_table($this->table)->find_one($id);
		}

		public function getAll()
		{
			return ORM::for_table($this->table)->find_many();
		}

		public function getDataCount($array)
		{
			return count(ORM::for_table($this->table)->where($array)->find_many());
		}
	}

?>
