<?php
	class Model_Professional extends Model
	{
		protected $table = 'Persona';
		protected $limit=25;
		public function getListProfessional($pag=0)
		{
			return ORM::for_table($this->table)->join("Moneda",array("Moneda.idMoneda","=","Persona.idMoneda"))->join("Usuario",array("Usuario.idPersona","=","Persona.idPersona"))->where("visible","Si")->limit($this->limit)->offset($this->limit*$pag)->find_many();
		}
		public function getProfession($id) //Profesion por ID de Persona
		{
			return ORM::for_table("Profesion")->join("PersonaProfesion",array("PersonaProfesion.idProfesion","=","Profesion.idProfesion"))->where("PersonaProfesion.idPersona",$id)->order_by_asc("principal")->limit(1)->find_one();
		}
		public function getWorkActivity($id) //por id Actividad Laboral 
		{
			return ORM::for_table("ActividadLaboral")->join("Persona",array("Persona.idActividadLaboral","=","ActividadLaboral.idActividadLaboral"))->find_one($id);
		}
		public function getSkills($id) //habilidades por ID de Persona
		{
			return ORM::for_table("Habilidad")->join("PersonaHabilidad",array("PersonaHabilidad.idHabilidad","=","Habilidad.idHabilidad"))->where("PersonaHabilidad.idPersona",$id)->limit(50)->find_many();
		}
		public function getAchievement($id) //Logros por ID de Persona
		{
			return ORM::for_table("Logro")->join("PersonaLogro",array("PersonaLogro.idLogro","=","Logro.idLogro"))->where("PersonaLogro.idPersona",$id)->find_many();
		}
		public function getStudies($id) //Estudios por ID de Persona
		{
			return ORM::for_table("Estudio")->where("idPersona",$id)->limit(50)->find_many();
		}
		public function getExperience($id) //Estudios por ID de Persona
		{
			return ORM::for_table("Experiencia")->where("idPersona",$id)->limit(50)->find_many();
		}
		public function getPais($id)
		{
			return ORM::for_table("Experiencia")->where("idPersona",$id)->limit(50)->find_many();
		}
		// Genericas
		public function putProfessional()
		{
			return ORM::for_table($this->table)->create();
		}
	}
