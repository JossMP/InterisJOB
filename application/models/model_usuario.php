<?php
	class Model_Usuario extends Model
	{
		protected $table = 'Usuario';
		
		public function getUserExist($login, $password)
		{
			return ORM::for_table($this->table)->where(array("user"=>$login, "password"=>md5($password), "estado"=>"Activo"))->find_one();
		}
		public function getPersona($id)
		{
			return ORM::for_table($this->table)->join('Persona', array('Persona.idPersona', '=', 'Usuario.idPersona'))->find_one($id);
		}
		public function getEmpresa($id)
		{
			return ORM::for_table($this->table)->join('Empresa', array('Empresa.idEmpresa', '=', 'Usuario.idEmpresa'))->find_one($id);
		}
		public function getUser($id)
		{
			return ORM::for_table($this->table)->where("estado", "Activo")->find_one($id);
		}
		public function getUserToken($token)
		{
			return ORM::for_table($this->table)->where( array("token"=>$token, "estado"=>"Activo") )->find_one();
		}
		public function getUserCode($code)
		{
			return ORM::for_table($this->table)->where( array("code"=>$code, "estado"=>"Inactivo") )->find_one();
		}
		//Funciones Genericas
		public function putUsuario()
		{
			return ORM::for_table($this->table)->create();
		}
		
		public function checkUser($login)
		{
			return ORM::for_table($this->table)->where_like("user", $login )->count();
		}
		public function checkEmail($email)
		{
			return ORM::for_table($this->table)->where_like( "email", $email )->count();
		}
	}
