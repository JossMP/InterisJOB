-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.26-MariaDB - MariaDB Server
-- SO del servidor:              Linux
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- Volcando datos para la tabla interis.DestinoMensaje: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `DestinoMensaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `DestinoMensaje` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Empresa: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Empresa` DISABLE KEYS */;
INSERT INTO `Empresa` (`idEmpresa`, `nombre`, `web`, `logo`, `descripcion`, `lat`, `lng`, `visible`) VALUES
	(1, 'InterisJob SAC', 'interisjob.com', '20170920015206596.png', 'Empresa de RR.HH.', NULL, NULL, 'Si');
/*!40000 ALTER TABLE `Empresa` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Estudio: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Estudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `Estudio` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Experiencia: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Experiencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `Experiencia` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Habilidad: ~17 rows (aproximadamente)
/*!40000 ALTER TABLE `Habilidad` DISABLE KEYS */;
INSERT INTO `Habilidad` (`idHabilidad`, `habilidad`, `descripcion`) VALUES
	(1, 'PHP', NULL),
	(2, 'SQL', NULL),
	(3, 'MySQL', NULL),
	(4, 'jQuery', NULL),
	(5, 'JavaScript', NULL),
	(6, 'CSS', NULL),
	(7, 'HTML', NULL),
	(8, 'Python', NULL),
	(9, 'C/C++', NULL),
	(10, 'C#', NULL),
	(11, '.NET', NULL),
	(12, 'Wordpress', NULL),
	(13, 'Joomla', NULL),
	(14, 'Laravel', NULL),
	(15, 'Code Night', NULL),
	(16, 'Perl', NULL),
	(17, 'Ruby', NULL);
/*!40000 ALTER TABLE `Habilidad` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Logro: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Logro` DISABLE KEYS */;
/*!40000 ALTER TABLE `Logro` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Mensaje: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Mensaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `Mensaje` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Notificacion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Notificacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `Notificacion` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Persona: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `Persona` DISABLE KEYS */;
INSERT INTO `Persona` (`idPersona`, `nombre`, `apellidos`, `sexo`, `visible`, `avatar`, `lat`, `lng`, `acercade`) VALUES
	(1, 'Josue', 'Mazco Puma', 'Masculino', 'Si', '20170912234957203.jpg', NULL, NULL, NULL);
/*!40000 ALTER TABLE `Persona` ENABLE KEYS */;

-- Volcando datos para la tabla interis.PersonaHabilidad: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `PersonaHabilidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `PersonaHabilidad` ENABLE KEYS */;

-- Volcando datos para la tabla interis.PersonaLogro: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `PersonaLogro` DISABLE KEYS */;
/*!40000 ALTER TABLE `PersonaLogro` ENABLE KEYS */;

-- Volcando datos para la tabla interis.PersonaProfesion: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `PersonaProfesion` DISABLE KEYS */;
/*!40000 ALTER TABLE `PersonaProfesion` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Profesion: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `Profesion` DISABLE KEYS */;
INSERT INTO `Profesion` (`idProfesion`, `titulo`, `descripcion`) VALUES
	(1, 'Ingeniero de Sistemas', NULL),
	(2, 'Ingeniero Civil', NULL),
	(3, 'Arquitecto', NULL);
/*!40000 ALTER TABLE `Profesion` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Tarea: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `Tarea` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tarea` ENABLE KEYS */;

-- Volcando datos para la tabla interis.Usuario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` (`idUsuario`, `user`, `password`, `estado`, `email`, `movil`, `token`, `code`, `idEmpresa`, `idPersona`, `acceso`, `create`, `tipo`) VALUES
	(1, 'user', md5('user'), 'Activo', 'jossmp@gmail.com', '971718714', '464ab36719855210b44ebbcf06f6261a', '4e1933c9489e820f1ae8675229580733', NULL, 1, '2017-09-28 04:40:38', '2017-09-26 21:05:47', 'Persona'),
	(2, 'demo', md5('demo'), 'Activo', 'admin@alecomled.com', '999999999', '63936a9471e47714e6dec2ca5fb59153', 'as', 1, NULL, '2017-09-28 03:29:51', '2017-09-28 03:24:28', 'Empresa');
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
