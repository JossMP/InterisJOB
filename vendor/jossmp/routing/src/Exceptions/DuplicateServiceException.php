<?php

namespace Mini\Exceptions;

use OverflowException;

/**
 * DuplicateServiceException
 *
 * Exception used for when a service is attempted to be registered that already exists
 */
class DuplicateServiceException extends OverflowException implements MiniExceptionInterface
{
}
