		<div class="container-main">
			<div class="container container-error">
				<div class="d-flex justify-content-center">	
					<div class="cont-center">	
						<h1 class="ce-title-error text-center my-4"><span>Error </span>404</h1>
						<div class="row cont-error">
							<div class="col-sm">
								<p>HTTP 404 Not Found , indica
								 que el host ha sido capaz de comunicarse con el servidor,pero no existe el recurso que ha sido pedido o el error puede surgir por una URL mal escrita.</p>
								<p>ERR_NAME_NOT_RESOLVED</p>
								<a href="#">Ir atrás</a>
							</div>
							<div class="ce-sv separate-vertical"></div>
							<div class="col-sm">
								<h5 class="text-title">O ir :</h5>
								<ul class="list-item">
									<li><a href="<?=$this->url("/");?>">Página principal</a></li>
									<li><a href="<?=$this->url("/professional");?>">Profesionales</a></li>
									<li><a href="<?=$this->url("/employment");?>">Empleos</a></li>									
									<li><a href="<?=$this->url("/company");?>">Empresas</a></li>
								</ul>							
							</div>
						</div>

					</div>
				</div>				
			</div>
		</div>