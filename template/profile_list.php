		<div class="container-main">
			<div class="container-new">
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-2">
						<h4 class="text-title text-rob py-4">Oferta de profesionales</h4>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-10">
						<!--cont-search-->
						<div class="row cont-search">
							<div class="col-sm-12 col-md-12 col-lg-10">
								<div class="cs-search my-2">
									<div class="hide show" id="collapse1">
										<form class="d-flex">
											<ul class="menu-filter">
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Actividad profesional</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Diseño y Multimedia</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Finanzas y Administración</label>
												</li>			
											</ul>
											<ul class="menu-filter">
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Hoteleria y Turismo</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Informática y Harware</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Ingeniería y Manufactura</label>
												</li>
											</ul>
											<ul class="menu-filter">
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> It Programacion</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Legal</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Marketin y Ventas</label>
												</li>
											</ul>											
											<ul class="menu-filter">
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Materia Prima</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Soporte Administrativo</label>
												</li>
												<li>
													<label><input type="checkbox" id="checkTodos" /><i class="icon-check-1"></i> TODOS</label>
												</li>
											</ul>
											<!--
											<ul class="menu-filter">
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Técnico</label>
												</li>
												<li>
													<label><input type="checkbox" name="lenguaje[]" /><i class="icon-check-1"></i> Traduccion y Contenido</label>
												</li>											
											</ul>
											-->
											<div>
												<button class="btn btn-success btn-sm m-3" name="btnFiltro" value="Filtro" id="filtrar">Filtrar</button>
											</div>
										</form>
									</div>
									<div class="hide" id="collapse2">
										<div class="input-group input-group-lg my-4">											
											<input type="text" class="form-control" placeholder="Buscar en Interisjob">
										</div>
									</div>
								</div>								
							</div>
							<div class="col-sm-12 col-md-12 col-lg-2">
								<div class="btn-group cs-radio my-4" data-toggle="buttons">									
									<label class="btn btn-light btn-lg active">
										<input type="radio" present="#collapse1" checked /><i class="icon-list-2"></i>
									</label>
									<label class="btn btn-light btn-lg">
										<input type="radio" present="#collapse2" /><i class="icon-search"></i>
									</label>
								</div>
							</div>
						</div>
						<!--/cont-search-->
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-2">
						<h6>Resultados</h6>
						<ul class="menu-details">
							<li><a href="#">100</a></li>							
						</ul>

						<h6>Orden de publicación</h6>
						<ul class="menu-details">
							<li><a href="#">Recientes <span>(20)</span></a></li>
							<li><a href="#">Pasadas <span>(500)</span></a></li>
						</ul>
						<h6>Categorías</h6>
						<ul class="menu-details">
							<li><a href="#">Actividad profesional <span>(300)</span></a></li>
							<li><a href="#">Diseño y Multimedia <span>(33)</span></a></li>
							<li><a href="#">Finanzas y Administración <span>(40)</span></a></li>
							<li><a href="#">Hoteleria y Turismo <span>(100)</span></a></li>
							<li><a href="#">Informática y Harware <span>(5)</span></a></li>
							<li><a href="#">...</a></li>
						</ul>
						<h6>Ubicaciones</h6>
						<ul class="menu-details">
							<li><a href="#">Norte</a></li>
							<li><a href="#">Sur</a></li>
							<li><a href="#">Este</a></li>
							<li><a href="#">Oeste</a></li>
						</ul>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-10">
						<!--container-list-->
						<div class="container-list">							
							<div class="cl-cont">								
								<?php
								foreach($this->profesionales->getListProfessional($this->request->pag) as $persona)
								{
								?>
								<div class="row">
									<div class="col-sm-12 col-md-3 col-lg-2">
										<div class="img-thumbnail it-perfil">
											<img src="<?=$this->url("images/avatar/crop_".$persona->avatar)?>" class="img-fluid" title="<?=$persona->nombre;?> <?=$persona->apellidos?>" alt="<?=$persona->nombre;?> <?=$persona->apellidos?>">
										</div>
										<div class="py-2">
											<?php foreach($this->profesionales->getAchievement($persona->idPersona) as $logro):?>
											<img src="<?=$this->url("images/achievement/".$logro->icon);?>" width="24" height="24" alt="<?=$logro->descripcion;?>" title="<?=$logro->descripcion;?>">
											<?php endforeach;?>
										</div>
									</div>
									<div class="col-sm-12 col-md-9 col-lg-10">							
										<h3>
											<a href="/professional/<?=$persona->user;?>">
												<span><?=$persona->nombre;?> <?=$persona->apellidos?></span>
											</a>
											<span class="rating" id="#">
												<i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i>
											</span>									
											<div class="float-right">
												<?php $ActividadLaboral = $this->profesionales->getWorkActivity($persona->idActividadLaboral); ?>
												<?php if($ActividadLaboral): ?>
												<img src="<?=$this->url("images/workactivity/".$ActividadLaboral->icon);?>" width="24" class="pull-right" title="<?=$ActividadLaboral->description;?>" alt="<?=$ActividadLaboral->description;?>">
												<?php endif; ?>
											</div>
										</h3>

										<span>Expectativa Salarial: <strong><?=$persona->salario;?> <?= $persona->moneda;?></strong></span>
										<?php 
										$profesion = $this->profesionales->getProfession($persona->idPersona);
										if($profesion): ?>
										<span>Profesión: <strong><?= $profesion->titulo;?></strong></span>
										<?php endif; ?>
										<?php
										$habilidades = $this->profesionales->getSkills($persona->idPersona);
										if($habilidades):
										?>
										<p><strong>Habilidades:</strong></p>
										<ul class="list-skills">
											<?php foreach($habilidades as $habilidad):?>
											<li><a><?= $habilidad->habilidad; ?></a></li>
											<?php endforeach;?>
											<li><a href="#" class="ls-plus"><i class="icon-plus"></i></a></li>
										</ul>
										<?php endif; ?>
										<span class="country"></span>
										<span class="dotted-left">
											&nbsp;Último login: <strong class="date"><?= $this->fn->getElapsed($persona->acceso);?></strong>
										</span>
										<span class="dotted-left">
											&nbsp;Registrado desde: <strong class="date"><?= $this->fn->getElapsed($persona->create);?></strong>
										</span>															
									</div>
								</div>
								<div  class="dotted-bottom"></div>
								<?php
								}
								?>								
							</div>	
						</div>
						<!--/container-list-->
					</div>
				</div>
			</div>
		</div>	