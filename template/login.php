		<div class="container-main">
			<div class="container container-input">
				<!--CONTENIDO-->
				<!--ingresar-->
				<div class="d-flex justify-content-center">	
					<div class="cont-center">

						<?php if($this->msg_error):?>
						<div class="alert alert-danger" role="alert">
							<?= $this->msg_error; ?>
						</div>
						<?php endif;?>
						<h2 class="title-div pb-2">Iniciar sesión<span> para acceder a tu cuenta</span></h2>

						<div class="row cont-login">
							<div class="col-sm">
								<form method="POST" action="<?= $this->url("login");?>">									
									<div class="form-group">
										<label for="email">Usuario o correo electrónico:</label>
										<input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario o email" value="<?=$this->request->usuario?>" required>
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Contraseña:</label>
										<input type="password" class="form-control" name="clave" id="clave" placeholder="contraseña" required>
									</div>
									<div class="form-check">
										<label class="form-check-label">
										  <input type="checkbox" class="form-check-input" name="remember">
										  Recordar contraseña
										</label>
									</div>							
									<div class="separator"></div>
									<button type="submit" class="btn btn-dark-blue float-right">Ingresar</button>
								</form>
							</div>
							<div class="cl-sv separate-vertical"></div>
							<div class="col-sm">
								<h6 class="text-title">O iniciar por :</h6>
								<ul class="menu-social-login my-5">
									<li><a href="#"><i class="icon-twitter"></i></a></li>
									<li><a href="#"><i class="icon-facebook"></i></a></li>
									<li><a href="#"><i class="icon-google"></i></a></li>
								</ul>								
								<div class="form-check">
									<label class="form-check-label">
									  <input type="checkbox" class="form-check-input" name="remember">
									  Recordar contraseña
									</label>
								</div>
								<h6 class="text-title">¿No puedo iniciar sesión?</h6>
								<a href="#">Recuperar cuenta</a>
								<h6 class="text-title">¿Aún no tienes una cuenta?</h6>
								<a href="#">Crear cuenta nueva</a>								
							</div>
						</div>

					</div>
				</div>
				<!--/ingresar-->
			</div>
		</div>
