		<div class="container-main">
			<div class="container container-input">
				<div class="d-flex justify-content-center">	
					<div class="cont-center" style="width:70%;">
						<h2 class="title-div pb-4">Registrarse <span>como empresa</span></h2>							
						<div class="row">
							<div class="col-sm">
								<form class="form-horizaontal">														
									<div class="form-group row">										
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="inputPassword" class="col-form-label font-weight-bold">Nombre empresa</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="text" class="form-control" id="inputPassword" placeholder="Nombre de la empresa">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="inputPassword" class="col-form-label font-weight-bold">Dirección</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="text" class="form-control" id="inputPassword" placeholder="Dirección">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="inputPassword" class="col-form-label font-weight-bold">Email</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="text" class="form-control" id="inputPassword" placeholder="Email">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="inputPassword" class="col-form-label font-weight-bold">Contraseña</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="password" class="form-control" id="inputPassword" placeholder="Contraseña">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="inputPassword" class="col-form-label font-weight-bold">Confirma contraseña</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="password" class="form-control" id="inputPassword" placeholder="Confirmar contraseña">
										</div>
									</div>
									<div class="form-check">
										<label class="form-check-label">
										  <input type="checkbox" class="form-check-input">
										  Al crear tu cuenta, estás aceptando los términos de servicio y la política de privacidad de Interisjob.
										</label>
									</div>
									<div class="d-flex justify-content-end">							
										<button type="submit" class="btn btn-dark-blue mx-auto">Ingresar</button>				
									</div>						
								</form>
							</div>
							<div class="cl-sv separate-vertical"></div>
							<div class="col-sm">																			
								<h5 class="text-title">¿Porqué registrarse como empresa?</h5>
								<ul class="list-item">
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
									<li>Lorem ipsum dolor sit amet.</li>
									<li>Lorem ipsum dolor sit amet, consectetur </li>
									<li>Lorem ipsum dolor sit amet, consectetur .Ut enim ad minim veniam.</li>
								</ul>
								<h5 class="text-title">¿Geolocalizar empresa?</h5>
								<ul class="list-item">
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
									<li>Lorem ipsum dolor sit amet.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>