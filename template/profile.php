		<div class="container-main">
			<div class="container container-input">
				<!--CONTENIDO-->
					<div class="row">
						<div class="col-sm-12 col-md-5 col-lg-3">
							<!--
							<div class="profile-box-img">
								<div class="pbi-img">
									<img src="<?=$this->url("images/avatar/crop_".$this->user->logo)?>"alt="avatar" width="30" height="30">
									<span><?= $this->user->nombre; ?></span>
								</div>								
								<div class="pbi-box-edit box-edit"><a><i class="icon-pencil"></i></a></div>
							</div>
							-->
						</div>
						<div class="col-sm-12 col-md-7 col-lg-9">
							<div class="row">
								<div class="col-sm">
									<ul class="menu-pagination">
										<li><a href="#">holas</a><i class="icon-right-open-1"></i></li>
										<li><a href="#">holas</a><i class="icon-right-open-1"></i></li>
										<li><a href="#">holas</a><i class="icon-right-open-1"></i></li>
									</ul>
								</div>
								<div class="col-sm">
									<ul class="nav justify-content-end menu-submenu">
										<li class="nav-item">
											<a class="nav-link active" href="#">new link</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#"><i class="icon-mail"></i></a>
										</li>
										<li class="nav-item">
											<a class="nav-link" href="#"><i class="icon-chat-1"></i></a>
										</li>
										<li class="nav-item">
											<a class="nav-link disabled" href="#"><i class="icon-flag-empty"></i></a>
										</li>
										<li class="nav-item dropdown">
											<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="icon-bell"></i>
											</a>
											<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 30px, 0px); top: 0px; left: 0px; will-change: transform;">
												<div class="dm-notification">
													notificar algo...													
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-md-5 col-lg-3">
							<div class="list-group profile-list-left" id="list-tab" role="tablist">
								<a class="list-group-item list-group-item-action active" id="list-profile1" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Mi perfil</a>
								<a class="list-group-item list-group-item-action" id="list-profile2" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Editar perfil</a>
								<a class="list-group-item list-group-item-action" id="list-profile3" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Certificar</a>								
							</div>
						</div>
						<div class="col-sm-12 col-md-7 col-lg-9">						
							<div class="tab-content profile-list-right" id="nav-tabContent">								
								<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-profile1">

									<!--container-profile-head-->
									<div class="container-profile-head">
										<div class="d-flex justify-content-center">										
											<img src="<?=$this->url("images/avatar/crop_".$this->user->logo)?>" class="cph-img d-block" alt="avatar" width="100" height="100">																						
											<h2 class="cph-name text-title"><?= $this->user->nombre; ?></h2>																						
										</div>
									</div>
									<!--/container-profile-head-->

									<!--container-profile-->
									<div class="container-profile">
										<div class="cp-top d-flex justify-content-between">
											<div class="cp-title">
												<a href="#profile1" id="idProfile" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="icon-down-open-1"></i> Habilidades</a>
											</div>
											<div class="cp-edit"><a href="#"><i class="icon-pencil"></i></a></div>
										</div>
										<div class="cp-cont collapse show" id="profile1">
											cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim helvetica, craft beer labore wes proident.
										</div>
									</div>
									<!--/container-profile-->

									<!--container-profile-->
									<div class="container-profile">
										<div class="cp-top d-flex justify-content-between">
											<div class="cp-title">
												<a href="#profile2" id="idProfile" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="icon-down-open-1"></i> Profesión</a>
											</div>
											<div class="cp-edit"><a href="#"><i class="icon-pencil"></i></a></div>
										</div>
										<div class="cp-cont collapse show" id="profile2">
											cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim helvetica, craft beer labore wes proident.
										</div>
									</div>
									<!--/container-profile-->

									<!--container-profile-->
									<div class="container-profile">
										<div class="cp-top d-flex justify-content-between">
											<div class="cp-title">
												<a href="#profile3" id="idProfile" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="icon-down-open-1"></i> Experiencia laboral</a>
											</div>
											<div class="cp-edit"><a href="#"><i class="icon-pencil"></i></a></div>
										</div>
										<div class="cp-cont collapse show" id="profile3">
											cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim helvetica, craft beer labore wes proident.
										</div>
									</div>
									<!--/container-profile-->

									<!--container-profile-->
									<div class="container-profile">
										<div class="cp-top d-flex justify-content-between">
											<div class="cp-title">
												<a href="#profile4" id="idProfile" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample"><i class="icon-down-open-1"></i> Ubicación</a>
											</div>
											<div class="cp-edit"><a href="#"><i class="icon-pencil"></i></a></div>
										</div>
										<div class="cp-cont collapse show" id="profile4">
											<div id="map" style="width: 100%; height: 400px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="http://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div><div class="gm-err-title">Se ha producido un error.</div><div class="gm-err-message">Esta página no ha cargado Google Maps correctamente. Descubre los detalles técnicos del problema en la consola de JavaScript.</div></div></div></div></div>
										</div>
									</div>
									<!--/container-profile-->

								</div>
								<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile2">
									<!--profile-edit-->
									<div class="profile-edit">
										<div class="pe-title">
											<div class="title-line">																							
												<h3 class="tl-title text-title">Mis datos:</h3><hr class="tl-line">
											</div>
										</div>
										<div class="pe-cont">
											<form>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="" class="col-form-label">Nombre</label>
														<input type="text" class="form-control" placeholder="Nombre">
													</div>
													<div class="form-group col-md-6">
														<label for="" class="col-form-label">Apellidos</label>
														<input type="text" class="form-control" placeholder="Apellidos">
													</div>
												</div>
												<div class="form-group">
													<label for="" class="col-form-label">Biografía (Breve descripción)</label>
													<textarea class="form-control" rows="3" placeholder="Tu biografía"></textarea>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4">
														<label for="" class="col-form-label">Fecha de nacimiento :</label>
														<input type="text" class="form-control">
													</div>
													<div class="form-group col-md-4">
														<label for="" class="col-form-label">Sexo :</label>
														<select class="form-control">
															<option>MASCULINO</option>
															<option>FEMENINO</option>
														</select>
													</div>
													<div class="form-group col-md-4">
														<label for="" class="col-form-label">Estado civil: </label>
														<select class="form-control">
															<option>SOLTERO</option>
															<option>CASADO</option>
														</select>
													</div>
												</div>
												<div class="form-group">
													<label for="" class="col-form-label">Lema / Slogan</label>
													<input type="text" class="form-control" placeholder="Ejemplo: Desarrollo de Software a Medida">
												</div>
												<div class="form-group">
													<button class="btn btn-success float-right my-2">Guardar</button>
												</div>					
											</form>										
										</div>
									</div>																		
									<!--profile-edit-->

									<!--profile-edit-->
									<div class="profile-edit">
										<div class="pe-title">											
											<div class="title-line">																							
												<h3 class="tl-title text-title">Habilidades :</h3><hr class="tl-line">
											</div>
										</div>
										<div class="pe-cont">
											<form>
												<div class="form-group">
													<label for="" class="col-form-label">Añadir nuevas habilidades :</label>
													<input type="text" class="form-control" placeholder="">
												</div>
												<div class="form-group">
													<label for="" class="col-form-label">Mis habilidades</label>
													<ul class="list-skills">
														<li><a>PHP</a></li>
														<li><a>SQL</a></li>
														<li><a>MySQL</a></li>
														<li><a>jQuery</a></li>
														<li><a>.NET</a></li>
														<li><a>Wordpress</a></li>
														<li><a href="#" class="ls-plus"><i class="icon-plus"></i></a></li>
													</ul>
												</div>					
											</form>	
										</div>
									</div>																		
									<!--profile-edit-->

									<!--profile-edit-->
									<div class="profile-edit">
										<div class="pe-title">											
											<div class="title-line">																							
												<h3 class="tl-title text-title">Profesión :</h3><hr class="tl-line">
											</div>
										</div>
										<div class="pe-cont">
											<!--pe-cont-->
											<label for="" class="col-form-label my-3">Estudios concluidos / profesiones :</label>
											<button class="btn btn-outline-warning btn-sm my-4"><i class="icon-plus"></i></button>
											<table class="table table table-striped table-hover">
												<thead>
													<tr>
														<th></th>
														<th>Institución</th>
														<th>Profesión</th>
														<th>Desde</th>
														<th>Hasta</th>
														<th style="width:15%;"></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th scope="row" >1</th>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>															
															<button type="button" class="btn btn-success btn-sm"><i class="icon-ok"></i></button>
															<button type="button" class="btn btn-danger btn-sm"><i class="icon-cancel"></i></button>
														</td>
													</tr>
													<tr>
														<th scope="row" >2</th>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>															
															<button type="button" class="btn btn-success btn-sm"><i class="icon-ok"></i></button>
															<button type="button" class="btn btn-danger btn-sm"><i class="icon-cancel"></i></button>
														</td>
													</tr>
												</tbody>
											</table>
											<!--/pe-cont-->	
											<!--pe-cont-->
											<label for="" class="col-form-label my-3">Estudios cursados / charlas / seminarios :</label>
											<button class="btn btn-outline-warning btn-sm my-4"><i class="icon-plus"></i></button>
											<table class="table table table-striped table-hover">
												<thead>
													<tr>
														<th></th>
														<th>Nominación / Nombre</th>
														<th>Fecha</th>
														<th style="width:15%;"></th>				
													</tr>
												</thead>
												<tbody>
													<tr>
														<th scope="row" >1</th>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>				
														<td>															
															<button type="button" class="btn btn-success btn-sm"><i class="icon-ok"></i></button>
															<button type="button" class="btn btn-danger btn-sm"><i class="icon-cancel"></i></button>
														</td>
													</tr>
												</tbody>
											</table>
											<!--/pe-cont-->										
										</div>
									</div>																		
									<!--profile-edit-->

									<!--profile-edit-->
									<div class="profile-edit">
										<div class="pe-title">											
											<div class="title-line">																																			
												<div class="title-line">																							
													<h3 class="tl-title text-title">Experiencia laboral :</h3><hr class="tl-line">
												</div>
											</div>
										</div>
										<div class="pe-cont">
											<!--pe-cont-->
											<label for="" class="col-form-label my-3">Experiencia :</label>
											<button class="btn btn-outline-warning btn-sm my-4"><i class="icon-plus"></i></button>
											<table class="table table table-striped table-hover">
												<thead>
													<tr>
														<th></th>
														<th>Cargo</th>
														<th>Institución</th>														
														<th>Desde</th>
														<th>Hasta</th>
														<th style="width:15%;"></th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<th scope="row" >1</th>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>															
															<button type="button" class="btn btn-success btn-sm"><i class="icon-ok"></i></button>
															<button type="button" class="btn btn-danger btn-sm"><i class="icon-cancel"></i></button>
														</td>
													</tr>
													<tr>
														<th scope="row" >2</th>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>
															<input type="text" class="form-control" placeholder="">
														</td>
														<td>															
															<button type="button" class="btn btn-success btn-sm"><i class="icon-ok"></i></button>
															<button type="button" class="btn btn-danger btn-sm"><i class="icon-cancel"></i></button>
														</td>
													</tr>
												</tbody>
											</table>
											<!--/pe-cont-->
										</div>
									</div>																		
									<!--profile-edit-->

									<!--profile-edit-->
									<div class="profile-edit">
										<div class="pe-title">											
											<div class="title-line">																							
												<h3 class="tl-title text-title">Ubicación :</h3><hr class="tl-line">
											</div>
										</div>
										<div class="pe-cont">
											<form>	
												<div class="form-row">
													<div class="form-group col-md-3">
														<label for="" class="col-form-label">País :</label>
														<select class="form-control">
														</select>
													</div>
													<div class="form-group col-md-3">
														<label for="" class="col-form-label">Departamento :</label>
														<select class="form-control">
														</select>
													</div>
													<div class="form-group col-md-3">
														<label for="" class="col-form-label">Provincia: </label>
														<select class="form-control">
														</select>
													</div>
													<div class="form-group col-md-3">
														<label for="" class="col-form-label">Distrito: </label>
														<select class="form-control">
														</select>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4">
														<label for="" class="col-form-label">Latitud :</label>
														<input type="text" class="form-control">
													</div>
													<div class="form-group col-md-4">
														<label for="" class="col-form-label">Longitud :</label>		
														<input type="text" class="form-control">
													</div>
													<div class="form-group col-md-4">														
														<button class="btn btn-warning float-right mt-4">GEO auto</button>
													</div>
												</div>	
												<div class="form-row">
													<div id="map" style="width: 100%; height: 400px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="http://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div><div class="gm-err-title">Se ha producido un error.</div><div class="gm-err-message">Esta página no ha cargado Google Maps correctamente. Descubre los detalles técnicos del problema en la consola de JavaScript.</div></div></div></div></div>
												</div>											
											</form>
										</div>
									</div>																		
									<!--profile-edit-->

								</div>
								<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-profile3">
									certificar...
								</div>								
							</div>
						</div>
					</div>
				<!--/CONTENIDO-->
			</div>
		</div>