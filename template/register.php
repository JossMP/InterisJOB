		<div class="container-main">
			<div class="container container-input">
				<h2 class="text-title py-2">Nuevo en Interisjob <strong>¡ Regístrate , es gratis !</strong></h2>
				<div class="row">
					<div class="col">
						<div class="rc-cont-min text-center">						
							<h3 class="text-green text-rob py-4">¿ Necesito <strong>contratar<br>a un profesional</strong> ?</h3>
							<h5 class="text-secondary py-3">Empresas</h5>
							<a href="register/company" class="btn btn-lg btn-success text-uppercase ">Registrar</a>
						</div>
					</div>
					<div class="col">
						<div class="rc-cont-min text-center">
							<h3 class="text-title text-rob py-4">¿ <strong>Publica</strong> tu hoja <br>de vida ?</h3>
							<h5 class="text-secondary py-3">job</h5>
							<a href="register/job" class="btn btn-lg btn-dark-blue text-uppercase">Registrar</a>
						</div>
					</div>
				</div>
			</div>
		</div>

