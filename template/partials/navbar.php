		<header class="container-top fixed-top">
			<div class="container">												
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="<?= $this->url();?>"><span style="color:#fff; font-size:1.5rem;letter-spacing:0.1rem">Interis<span style="color:#f8b032;">job</span></span></a>
					<div class="menu-bar navbar-toggler" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<div class="mb-line-1"></div>
						<div class="mb-line-2"></div>
						<div class="mb-line-3"></div>
					</div>
					<div class="collapse navbar-collapse" id="navbarSupportedContent" style="width:100%;">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="<?=$this->url("/");?>">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=$this->url("/professional");?>">Profesionales</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=$this->url("/employment");?>">Empleos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="<?=$this->url("/company");?>">Empresas</a>
							</li>
						</ul>
						<?php
						if( $this->session->isUser() && $this->session->get("tipo") == "Persona" )
						{
						?>
						<ul class="nav menu-submenu">
							<li class="nav-item">
								<a class="nav-link" href="#"><i class="icon-mail"></i></a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="icon-bell"></i>
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<div class="dm-notification">
										notificar algo...
									</div>
								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="<?=$this->url("images/avatar/crop_".$this->user->avatar)?>" class="rounded" alt="avatar" width="24" height="24">
									<strong><?= $this->user->user; ?></strong>
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="<?= $this->url("/profile");?>">Mi perfil</a>
									<a class="dropdown-item" href="<?= $this->url("");?>">Menu Prof 02</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?= $this->url("logout");?>">Salir</a>
								</div>
							</li>
						</ul>
						<?php
						}
						else if( $this->session->isUser() && $this->session->get("tipo") == "Empresa" )
						{
						?>
						<ul class="nav menu-submenu">
							<li class="nav-item">
								<a class="nav-link" href="#"><i class="icon-mail"></i></a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="icon-bell"></i>
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<div class="dm-notification">
										notificar algo...
									</div>
								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="<?=$this->url("images/avatar/crop_".$this->user->logo)?>" class="rounded" alt="avatar" width="30" height="30">
									<strong><?= $this->user->nombre; ?></strong>
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="<?= $this->url("/profile");?>">Mi perfil</a>
									<a class="dropdown-item" href="<?= $this->url("");?>">Menu Empresa 02</a>
									<div class="dropdown-divider"></div>
									<a class="dropdown-item" href="<?= $this->url("logout");?>">Salir</a>
								</div>
							</li>
						</ul>
						<?php
						}
						else
						{
						?>
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
							<a class="nav-link" href="<?= $this->url("register");?>">Regístrate</a>
							</li>
							<li class="nav-item">
							<a class="nav-link" href="<?= $this->url("login");?>">Ingresa</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Idioma
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="#">Spanish</a>
									<a class="dropdown-item" href="#">English</a>
									<a class="dropdown-item" href="#">Others...</a>
								</div>
							</li>
						</ul>
						<?php }?>
					</div>
				</nav>
			</div>
		</header>
