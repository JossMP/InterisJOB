		<footer class="container-bottom">
			<div class="container">
				<div class="row">
					<div class="col">
						<h6 class="cb-title mt-5">Tipo de contrato</h6>
						<ul class="menu-foot">
							<li><a href="#">Tiempo Completo</a></li>
							<li><a href="#">Tiempo Parcial</a></li>
							<li><a href="#">Medio Tiempo</a></li>
							<li><a href="#">Pasantia</a></li>
							<li><a href="#">Freelance</a></li>							
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Actividad profesional</h6>
						<ul class="menu-foot">
							<li><a href="#">Diseño y Multimedia</a></li>
							<li><a href="#">Finanzas y Administración</a></li>
							<li><a href="#">Hoteleria y Turismo</a></li>
							<li><a href="#">Informática y Harware</a></li>
							<li><a href="#">It Programacion</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Marketin Y Ventas</a></li>
							<li><a href="#">Materia Prima</a></li>
							<li><a href="#">Soporte Administrativo</a></li>
							<li><a href="#">Técnico</a></li>
							<li><a href="#">Traducción y Contenido</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Acerca de</h6>
						<ul class="menu-foot">
							<li><a href="#">Sobre Nosotros</a></li>
							<li><a href="#">Politicas de Privacidad</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Soporte</h6>
						<ul class="menu-foot">
							<li><a href="#">Ayuda</a></li>
							<li><a href="#">Contactar</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col-12">
						<div class="cb-foot">
							<div class="row">
								<div class="col">
									<p class="mt-2">© Copyright <?= date('Y') ?> - Interisjob - Todos los derechos reservados</p>
								</div>
								<div class="col">
									<ul class="menu-social mt-3 text-right">
										<li><a href="#"><i class="icon-twitter"></i></a></li>
										<li><a href="#"><i class="icon-facebook"></i></a></li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--========== /container-bottom==========-->
		<script src="<?= $this->url("js/jquery-3.2.1.min.js");?>"></script>
		<script src="<?= $this->url("js/popper.min.js");?>"></script>
		<script src="<?= $this->url("js/bootstrap.min.js");?>"></script>
		<script src="<?= $this->url("js/interisjob.js");?>"></script>
