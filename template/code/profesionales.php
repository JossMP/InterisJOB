<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link href="css/bootstrap-modif.css" rel="stylesheet"/>
		<link href="css/interisjob.css" rel="stylesheet"/>
		<link href="font/fontello.css" rel="stylesheet">

		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

	</head>
	<body>

		<!--========== /container-top ==========-->
		<div class="container-top fixed-top">
			<div class="container">												
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#"><span style="color:#fff; font-size:1.5rem;letter-spacing:0.1rem">Interis<span style="color:#f8b032;">job</span></span></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<i class="icon-menu-3"></i>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent" style="width:100%;">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="#">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Profesionales</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Empleos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Empresas</a>
							</li>
						</ul>
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
							<a class="nav-link" href="#">Regístrate</a>
							</li>
							<li class="nav-item">
							<a class="nav-link" href="#">Ingresa</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Idioma
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="#">Spanish</a>
									<a class="dropdown-item" href="#">English</a>
									<a class="dropdown-item" href="#">Others...</a>
								</div>
							</li>
				    	</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--========== /container-top ==========-->	

		<!--========== container-title ==========-->
		<div class="container-title">
			<div class="container">
				<div class="row">
					<div class="col-sm">			
						<h3 class="ct-title text-rob my-4">Oferta de profesionales</h3>
					</div>
					<div class="col-sm">
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-title ==========-->

		<!--========== /container-main-min ==========-->		
		<div class="container-main">
			<div class="cm-orange cm-orange-min"></div>
			<div class="container cm-elevate cm-elevate-min">
				<!--CONTENIDO-->
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-3">
						<div class="box-filter">
							<h5 class="text-title text-center my-3">Actividad profesional</h5>					
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="1"> Diseno Y Multimedia
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="2"> Finanzas Y Administracion
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="3"> Hoteleria Y Turismo
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="4"> Informatica Y Harware
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="6"> Ingeniería Y Manufactura
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="5"> It Programacion
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="7"> Legal
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="8"> Marketin Y Ventas
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="9"> Materia Prima
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="10"> Soporte Administrativo
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="11"> Tecnico
							</label>
							<label class="checkbox col-12">
								<input type="checkbox" name="idActividadLaboral[]" value="12"> Traduccion Y Contenido
							</label>
						</div>
						<button class="btn btn-success btn-sm float-right my-2" name="btnFiltro" value="Filtro">Filtrar</button>					
					</div>
					<div class="col-sm-12 col-md-7 col-lg-9">
						<!--lista-->
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-2">
								<div class="img-thumbnail it-perfil">
									<img src="images/perfil/crop_20170912234957203.jpg" class="img-fluid" title="Josue Mazco Puma" alt="Josue Mazco Puma">
								</div>
								<div class="py-2">
									<img src="images/certified/01.png" width="25" height="25" alt="Profesional verificado por Workana">
									<img src="images/certified/top100.png" width="25" height="25" alt="Profesional rankeado top 100">
									<img src="images/certified/hero.png" width="25" height="25" alt="Profesional Hero">
								</div>
							</div>
							<div class="col-sm-12 col-md-9 col-lg-10">							
								<h3>
									<a href="./perfil/profesional/josue-mazco-puma-1">
										<span>Josue Mazco Puma</span>
									</a>
									<span class="rating" id="#">
										<i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i>
									</span>									
									<div class="float-right">
										<img src="images/areas/W.png" width="24" class="pull-right" title="it programacion" alt="it programacion">
									</div>
								</h3>

								<span>Expectativa Salarial: <strong>750.00 Soles</strong></span>
								<span>Profesión: <strong>Ingeniero Espacial</strong></span>							

								<p><strong>Habilidades:</strong></p>
								<ul class="list-skills">
									<li><a>PHP</a></li>																
									<li><a>MySQL</a></li>
									<li><a href="#" class="ls-plus"><i class="icon-plus"></i></a></li>
								</ul>														

								<span class="country"></span>

								<span class="dotted-left">
									&nbsp;Último login: <strong class="date">Hace 6 dias</strong>
								</span>
								<span class="dotted-left">
									&nbsp;Registrado desde: <strong class="date">Hace 9 dias</strong>
								</span>															
							</div>
						</div>
						<div  class="dotted-bottom"></div>
						<!--/lista-->
						<!--lista-->
						<div class="row">
							<div class="col-sm-12 col-md-3 col-lg-2">
								<div class="">
									<div class="img-thumbnail it-perfil">
										<img src="images/perfil/crop_20170912234957203.jpg" class="img-fluid" title="Josue Mazco Puma" alt="Josue Mazco Puma">
									</div>
									<div class="py-2">
										<img src="images/certified/01.png" width="28" height="28" alt="Profesional verificado por Workana">
										<img src="images/certified/top100.png" width="28" height="28" alt="Profesional rankeado top 100">
										<img src="images/certified/hero.png" width="28" height="28" alt="Profesional Hero">
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-9 col-lg-10">							
								<h3>
									<a href="./perfil/profesional/josue-mazco-puma-1">
										<span>Josue Mazco Puma</span>
									</a>
									<span class="rating" id="#">
										<i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i>
									</span>									
									<div class="float-right">
										<img src="images/areas/W.png" width="24" class="pull-right" title="it programacion" alt="it programacion">
									</div>
								</h3>

								<span>Expectativa Salarial: <strong>750.00 Soles</strong></span>
								<span>Profesión: <strong>Ingeniero Espacial</strong></span>							

								<p><strong>Habilidades:</strong></p>
								<ul class="list-skills">
									<li><a>PHP</a></li>																
									<li><a>MySQL</a></li>
									<li><a href="#" class="ls-plus"><i class="icon-plus"></i></a></li>
								</ul>														

								<span class="country"><i class="flag-pe"></i> <strong class="country-name"><span itemprop="addressRegion">Puno</span> - <span itemprop="addressLocality">Peru</span></strong></span>
								<span class="dotted-left">
									&nbsp;Último login: <strong class="date">Hace 6 dias</strong>
								</span>
								<span class="dotted-left">
									&nbsp;Registrado desde: <strong class="date">Hace 9 dias</strong>
								</span>															
							</div>
						</div>
						<div  class="dotted-bottom"></div>
						<!--/lista-->							
					</div>
				</div>
				<!--/CONTENIDO-->				
			</div>			
		</div>
		<!--========== /container-main-min ==========-->

		<!--========== container-section ==========-->
		<div class="container-section cs-orange">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="so-text my-4">Quiero unirme a ésta COMUNIDAD</h3>
					</div>
					<div class="col">
						<div class="d-flex justify-content-end">
							<button class="btn btn-primary b-left mt-4">CLICK AQUÍ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-section ==========-->

		<!--========== container-bottom ==========-->
		<div class="container-bottom">
			<div class="container">
				<div class="row">
					<div class="col">
						<h6 class="cb-title mt-5">Tipo de contrato</h6>
						<ul class="menu-foot">
							<li><a href="#">Tiempo Completo</a></li>
							<li><a href="#">Tiempo Parcial</a></li>
							<li><a href="#">Medio Tiempo</a></li>
							<li><a href="#">Pasantia</a></li>
							<li><a href="#">Freelance</a></li>							
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Actividad profesional</h6>
						<ul class="menu-foot">
							<li><a href="#">Diseño y Multimedia</a></li>
							<li><a href="#">Finanzas y Administración</a></li>
							<li><a href="#">Hoteleria y Turismo</a></li>
							<li><a href="#">Informática y Harware</a></li>
							<li><a href="#">It Programacion</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Marketin Y Ventas</a></li>
							<li><a href="#">Materia Prima</a></li>
							<li><a href="#">Soporte Administrativo</a></li>
							<li><a href="#">Técnico</a></li>
							<li><a href="#">Traducción y Contenido</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Acerca de</h6>
						<ul class="menu-foot">
							<li><a href="#">Sobre Nosotros</a></li>
							<li><a href="#">Politicas de Privacidad</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Soporte</h6>
						<ul class="menu-foot">
							<li><a href="#">Ayuda</a></li>
							<li><a href="#">Contactar</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col-12">
						<div class="cb-foot">
							<div class="row">
								<div class="col">
									<p class="mt-2">© Copyright 2017 - Interisjob - Todos los derechos reservados</p>
								</div>
								<div class="col">
									<ul class="menu-social mt-3 text-right">
										<li><a href="#"><i class="icon-twitter"></i></a></li>
										<li><a href="#"><i class="icon-facebook"></i></a></li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-bottom==========-->

	</body>
</html>