<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link href="css/bootstrap-modif.css" rel="stylesheet"/>
		<link href="css/interisjob.css" rel="stylesheet"/>
		<link href="font/fontello.css" rel="stylesheet">

		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

	</head>
	<body>

		<!--========== /container-top ==========-->
		<div class="container-top fixed-top">
			<div class="container">												
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#"><span style="color:#fff; font-size:1.5rem;letter-spacing:0.1rem">Interis<span style="color:#f8b032;">job</span></span></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<i class="icon-menu-3"></i>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent" style="width:100%;">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="#">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Profesionales</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Empleos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Empresas</a>
							</li>
						</ul>
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
							<a class="nav-link" href="#">Regístrate</a>
							</li>
							<li class="nav-item">
							<a class="nav-link" href="#">Ingresa</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Idioma
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="#">Spanish</a>
									<a class="dropdown-item" href="#">English</a>
									<a class="dropdown-item" href="#">Others...</a>
								</div>
							</li>
				    	</ul>
					</div>
				</nav>
			</div>
		</div>
		<!--========== /container-top ==========-->	

		<!--========== container-title ==========-->
		<div class="container-title">
			<div class="container">
				<div class="row">
					<div class="col-sm">			
						<h3 class="ct-title text-rob my-4">Ventanas Básicas</h3>
					</div>
					<div class="col-sm">
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-title ==========-->

		<!--========== /container-main-min ==========-->		
		<div class="container-main">
			<div class="cm-orange cm-orange-min"></div>
			<div class="container cm-elevate cm-elevate-min">
				<!--CONTENIDO-->

				<!--ingresar-->
				<div class="d-flex justify-content-center">	
					<div class="cont-center">				
						<form>
							<h2 class="text-title py-2">Ingresar</h2>
							<div class="form-group">
								<label for="exampleInputEmail1">E-mail</label>
								<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="email">						
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Contraseña</label>
								<input type="password" class="form-control" id="exampleInputPassword1" placeholder="contraseña">
							</div>
							<div class="form-check">
								<label class="form-check-label">
								  <input type="checkbox" class="form-check-input">
								  Recordar contraseña
								</label>
							</div>
							<button type="submit" class="btn btn-dark-blue float-right">Ingresar</button>
						</form>
					</div>
				</div>
				<!--/ingresar-->

				<!--Elige registrar-->
				<h2 class="text-title py-2">Nuevo en Interisjob <strong>¡ Regístrate , es gratis !</strong></h2>
				<div class="row">
					<div class="col">
						<div class="rc-cont-min text-center">						
							<h3 class="text-green text-rob py-4">¿ Necesito <strong>contratar<br>a un profesional</strong> ?</h3>
							<h5 class="text-secondary py-3">Empresas</h5>
							<a href="" class="btn btn-lg btn-success text-uppercase ">Registrar</a>
						</div>
					</div>
					<div class="col">
						<div class="rc-cont-min text-center">
							<h3 class="text-title text-rob py-4">¿ <strong>Publica</strong> tu hoja <br>de vida ?</h3>
							<h5 class="text-secondary py-3">job</h5>
							<a href="" class="btn btn-lg btn-dark-blue text-uppercase">Registrar</a>
						</div>
					</div>
				</div>
				<!--/Elige registrar-->

				<!--Registrar usuario empresario-->
				<div class="d-flex justify-content-center">	
					<div class="cont-center">	
						<form class="form-horizaontal">
							<h2 class="text-title py-2">Registrarse como empresa</h2>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Nombre empresa</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Nombre de la empresa">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Dirección</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Dirección">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Email</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Email">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Contraseña</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Contraseña">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Confirma contraseña</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Confirmar contraseña">
								</div>
							</div>
							<div class="form-check">
								<label class="form-check-label">
								  <input type="checkbox" class="form-check-input">
								  Al crear tu cuenta, estás aceptando los términos de servicio y la política de privacidad de Interisjob.
								</label>
							</div>
							<div class="d-flex justify-content-end">							
								<button type="submit" class="btn btn-dark-blue mx-auto">Ingresar</button>				
							</div>						
						</form>
					</div>
				</div>
				<!--/Registrar usuario empresario-->

				<!--Registrar usuario profesional-->
				<div class="d-flex justify-content-center">	
					<div class="cont-center">	
						<form class="form-horizaontal">
							<h2 class="text-title py-2">Registrarse como Profesional</h2>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Nombre</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Nombre">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Apellidos</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Apellidos">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Email</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Email">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Contraseña</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Contraseña">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputPassword" class="col-sm-3 col-form-label font-weight-bold">Confirma contraseña</label>
								<div class="col-sm-6">
									<input type="password" class="form-control" id="inputPassword" placeholder="Confirmar contraseña">
								</div>
							</div>
							<div class="form-check">
								<label class="form-check-label">
								  <input type="checkbox" class="form-check-input">
								  Al crear tu cuenta, estás aceptando los términos de servicio y la política de privacidad de Interisjob.
								</label>
							</div>
							<div class="d-flex justify-content-end">							
								<button type="submit" class="btn btn-dark-blue mx-auto">Ingresar</button>				
							</div>						
						</form>
					</div>
				</div>
				<!--/Registrar usuario profesional-->

				<!--/CONTENIDO-->
			</div>			
		</div>
		<!--========== /container-main-min ==========-->

		<!--========== container-section ==========-->
		<div class="container-section cs-orange">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="so-text my-4">Quiero unirme a ésta COMUNIDAD</h3>
					</div>
					<div class="col">
						<div class="d-flex justify-content-end">
							<button class="btn btn-dark-blue b-left mt-4">CLICK AQUÍ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-section ==========-->

		<!--========== container-bottom ==========-->
		<div class="container-bottom">
			<div class="container">
				<div class="row">
					<div class="col">
						<h6 class="cb-title mt-5">Tipo de contrato</h6>
						<ul class="menu-foot">
							<li><a href="#">Tiempo Completo</a></li>
							<li><a href="#">Tiempo Parcial</a></li>
							<li><a href="#">Medio Tiempo</a></li>
							<li><a href="#">Pasantia</a></li>
							<li><a href="#">Freelance</a></li>							
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Actividad profesional</h6>
						<ul class="menu-foot">
							<li><a href="#">Diseño y Multimedia</a></li>
							<li><a href="#">Finanzas y Administración</a></li>
							<li><a href="#">Hoteleria y Turismo</a></li>
							<li><a href="#">Informática y Harware</a></li>
							<li><a href="#">It Programacion</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Marketin Y Ventas</a></li>
							<li><a href="#">Materia Prima</a></li>
							<li><a href="#">Soporte Administrativo</a></li>
							<li><a href="#">Técnico</a></li>
							<li><a href="#">Traducción y Contenido</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Acerca de</h6>
						<ul class="menu-foot">
							<li><a href="#">Sobre Nosotros</a></li>
							<li><a href="#">Politicas de Privacidad</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Soporte</h6>
						<ul class="menu-foot">
							<li><a href="#">Ayuda</a></li>
							<li><a href="#">Contactar</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col-12">
						<div class="cb-foot">
							<div class="row">
								<div class="col">
									<p class="mt-2">© Copyright 2017 - Interisjob - Todos los derechos reservados</p>
								</div>
								<div class="col">
									<ul class="menu-social mt-3 text-right">
										<li><a href="#"><i class="icon-twitter"></i></a></li>
										<li><a href="#"><i class="icon-facebook"></i></a></li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-bottom==========-->

	</body>
</html>