<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link href="css/bootstrap-modif.css" rel="stylesheet"/>
		<link href="css/interisjob.css" rel="stylesheet"/>
		<link href="font/fontello.css" rel="stylesheet">

		<script src="js/jquery-3.2.1.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

	</head>
	<body>

		<!--========== /container-top ==========-->
		<div class="container-top fixed-top clearfix">
			<div class="container">												
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
					<a class="navbar-brand" href="#"><span style="color:#fff; font-size:1.5rem;letter-spacing:0.1rem">Interis<span style="color:#f8b032;">job</span></span></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<i class="icon-menu-3"></i>	
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent" style="width:100%;">
						<ul class="navbar-nav mr-auto">
							<li class="nav-item">
								<a class="nav-link" href="#">Home</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Profesionales</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Empleos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">Empresas</a>
							</li>
						</ul>
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
							<a class="nav-link" href="#">Regístrate</a>
							</li>
							<li class="nav-item">
							<a class="nav-link" href="#">Ingresa</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Idioma
								</a>
								<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
									<a class="dropdown-item" href="#">Spanish</a>
									<a class="dropdown-item" href="#">English</a>
									<a class="dropdown-item" href="#">Others...</a>
								</div>
							</li>
				    	</ul>
					</div>
				</nav>
			</div>
		</div>
		<div class="clearfix"></div>
		<!--========== /container-top ==========-->	

		<!--========== container-carousel ==========-->
		<div class="container-carousel">
			<div class="container">								
				<div class="row">
					<div class="col-sm-6">			
						<h1 class="cc-title text-center">Búsque su siguiente trabajo</h1>
						<h3 class="text-gray text-center text-rob tr-1">En este interactivo mapa.</h3>		
					</div>
					<div class="col-sm-6">	
						<div class="carousel slide py-4" id="carouselExampleIndicators" data-ride="carousel">
						  <ol class="carousel-indicators">
						    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>								    
						  </ol>
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img class="" src="images/portada/new-e1.jpg" alt="First slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e2.jpg" alt="Second slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e3.jpg" alt="Third slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e4.png" alt="Third slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e5.png" alt="Third slide">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
				</div>							
			</div>
		</div>
		<!--========== /container-carousel ==========-->

		<!--========== /container-main ==========-->		
		<div class="container-main">
			<div class="cm-orange"></div>
			<div class="container cm-elevate">
				<!--CONTENIDO-->
					contenido...
				<!--/CONTENIDO-->
			</div>			
		</div>
		<!--========== /container-main ==========-->

		<!--========== container-section ==========-->
		<div class="container-section cs-darkblue">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h1 class="sdb-text my-5 text-rob tr-1">Búsque su siguiente trabajo.</h1>
					</div>
					<div class="col-sm-4">
						<button class="btn btn-success btn-lg btn-block my-5">Buscar</button>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-section ==========-->

		<!--========== container-mid ==========-->
		<div class="container-mid">
			<div class="container">
				Subcontenido de presentación...
			</div>
		</div>
		<!--========== /container-mid ==========-->

		<!--========== container-section ==========-->
		<div class="container-section cs-orange">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="so-text my-4">Quiero unirme a ésta COMUNIDAD</h3>
					</div>
					<div class="col">
						<div class="d-flex justify-content-end">
							<button class="btn btn-dark-blue b-left mt-4">CLICK AQUÍ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-section ==========-->

		<!--========== container-bottom ==========-->
		<div class="container-bottom">
			<div class="container">
				<div class="row">
					<div class="col">
						<h6 class="cb-title mt-5">Tipo de contrato</h6>
						<ul class="menu-foot">
							<li><a href="#">Tiempo Completo</a></li>
							<li><a href="#">Tiempo Parcial</a></li>
							<li><a href="#">Medio Tiempo</a></li>
							<li><a href="#">Pasantia</a></li>
							<li><a href="#">Freelance</a></li>							
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Actividad profesional</h6>
						<ul class="menu-foot">
							<li><a href="#">Diseño y Multimedia</a></li>
							<li><a href="#">Finanzas y Administración</a></li>
							<li><a href="#">Hoteleria y Turismo</a></li>
							<li><a href="#">Informática y Harware</a></li>
							<li><a href="#">It Programacion</a></li>
							<li><a href="#">Legal</a></li>
							<li><a href="#">Marketin Y Ventas</a></li>
							<li><a href="#">Materia Prima</a></li>
							<li><a href="#">Soporte Administrativo</a></li>
							<li><a href="#">Técnico</a></li>
							<li><a href="#">Traducción y Contenido</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Acerca de</h6>
						<ul class="menu-foot">
							<li><a href="#">Sobre Nosotros</a></li>
							<li><a href="#">Politicas de Privacidad</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col">
						<h6 class="cb-title mt-5">Soporte</h6>
						<ul class="menu-foot">
							<li><a href="#">Ayuda</a></li>
							<li><a href="#">Contactar</a></li>
							<li><a href="#">Términos del Servicio</a></li>
							<li><a href="#">Acuerdo de Usuario</a></li>
						</ul>
					</div>
					<div class="col-12">
						<div class="cb-foot">
							<div class="row">
								<div class="col">
									<p class="mt-2">© Copyright 2017 - Interisjob - Todos los derechos reservados</p>
								</div>
								<div class="col">
									<ul class="menu-social mt-3 text-right">
										<li><a href="#"><i class="icon-twitter"></i></a></li>
										<li><a href="#"><i class="icon-facebook"></i></a></li>
									</ul>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-bottom==========-->

	</body>
</html>
