		<div class="container-main">
			<div class="container container-input">
				<div class="d-flex justify-content-center">	
					<div class="cont-center" style="width:70%;">
						<h2 class="title-div pb-4">Registrarse <span>como profesional</span></h2>
						<?php if($this->msg_error):?>
						<div class="alert alert-danger" role="alert">
							<?= $this->msg_error;?>
						</div>
						<?php endif;?>
						<div class="row">
							<div class="col-sm">
								<form class="form-horizaontal" method="POST">
									<input type="hidden" id="crsf" name="crsf" value="<?=$this->session->get("csrf");?>">
									<div class="form-group row">										
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="nombre" class="col-form-label font-weight-bold">Nombre</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombres" value="<?=$this->request->nombre;?>" required>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="apellido" class="col-form-label font-weight-bold">Apellidos</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="text" class="form-control" id="apellidos" name="apellidos" placeholder="Apellidos" value="<?=$this->request->apellidos;?>" required>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="email" class="col-form-label font-weight-bold">Email</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?=$this->request->email;?>" required>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="user" class="col-form-label font-weight-bold">Usuario</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="text" class="form-control" id="user" name="user" placeholder="Usuario" value="<?=$this->request->user;?>" required>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="clave" class="col-form-label font-weight-bold">Contraseña</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="password" class="form-control" id="clave" name="clave" placeholder="Contraseña" required>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12 col-md-4 col-lg-4">
											<label for="clave2" class="col-form-label font-weight-bold">Confirma contraseña</label>
										</div>
										<div class="col-sm-12 col-md-8 col-lg-8">
											<input type="password" class="form-control" id="clave2" name="clave2" placeholder="Confirmar contraseña" required>
										</div>
									</div>
									<div class="form-check">
										<label class="form-check-label">
										  <input type="checkbox" name="terminos" class="form-check-input" checked>
										  Al crear tu cuenta, estás aceptando los <a href="<?=$this->url("policies/terms")?>">términos de servicio</a> y la <a href="<?=$this->url("policies/privacy")?>">política de privacidad</a> de Interisjob.
										</label>
									</div>
									<div class="d-flex justify-content-end">							
										<button type="submit" class="btn btn-dark-blue mx-auto">Registrarse</button>
									</div>
								</form>
							</div>
							<div class="cl-sv separate-vertical"></div>
							<div class="col-sm">																			
								<h5 class="text-title">¿Porqué registrarse como profesional?</h5>
								<ul class="list-item">
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
									<li>Lorem ipsum dolor sit amet.</li>
									<li>Lorem ipsum dolor sit amet, consectetur </li>
									<li>Lorem ipsum dolor sit amet, consectetur .Ut enim ad minim veniam.</li>
								</ul>
								<h5 class="text-title">¿Geolocalizar profesional?</h5>
								<ul class="list-item">
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
									<li>Lorem ipsum dolor sit amet.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
