<!DOCTYPE html>
<html lang="es-ES">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<?= $this->partial('template/partials/header.php'); ?>
	</head>
	<body>
		<?= $this->partial('template/partials/navbar.php'); ?>

		<?php $this->yieldView();?>

		<?= $this->partial('template/partials/chatbox.php'); ?>
		<?= $this->partial('template/partials/footer.php'); ?>
	</body>
</html>
