		<!--========== container-carousel ==========-->
		<div class="container-carousel">
			<div class="container">								
				<div class="row">
					<div class="col-sm-6">			
						<h1 class="cc-title text-center">Búsque su siguiente trabajo</h1>
						<h3 class="text-gray text-center text-rob tr-1">En este interactivo mapa.</h3>		
					</div>
					<div class="col-sm-6">	
						<div class="carousel slide py-4" id="carouselExampleIndicators" data-ride="carousel">
						  <ol class="carousel-indicators">
						    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>								    
						  </ol>
						  <div class="carousel-inner">
						    <div class="carousel-item active">
						      <img class="" src="images/portada/new-e1.jpg" alt="First slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e2.jpg" alt="Second slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e3.jpg" alt="Third slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e4.png" alt="Third slide">
						    </div>
						    <div class="carousel-item">
						      <img class="" src="images/portada/new-e5.png" alt="Third slide">
						    </div>
						  </div>
						  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
					</div>
				</div>							
			</div>
		</div>
		<!--========== /container-carousel ==========-->

		<!--========== /container-main ==========-->		
		<div class="container-main">
			<div class="cm-orange"></div>
			<div class="container cm-elevate">
			
				<div class="row cm-cont">
				    <div class="col-sm-12 col-md-12 col-lg-6">
				        <div class="title-line">																							
							<h1 class="tl-title text-title text-rob tr-1">Mapa interactivo</h1>
						</div>
						<div class="row">
							<div class="col-md-3 text-center">
								<i class=" icon-location-5"></i>
							</div>
							<div class="col-md-9">
								<p>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit earum accusamus dignissimos dicta, enim repellat amet delectus mollitia cumque unde. Nihil praesentium velit soluta accusantium asperiores voluptas nesciunt, porro consequuntur.
								</p>
							</div>
						</div>
				    </div>
				    <div class="col-sm-12 col-md-12 col-lg-6">
				        <img src="images/portada/mapa-interactivo.jpg" style="width:100%;">
				        <button class="btn btn-dark-blue b-left mt-4">VER LISTA DE PROFESIONALES</button>
				    </div>
				</div>

			</div>			
		</div>
		<!--========== /container-main ==========-->

		<!--========== container-section ==========-->
		<div class="container-section cs-darkblue">
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h1 class="my-5 text-rob tr-1">Búsque su siguiente trabajo.</h1>
					</div>
					<div class="col-sm-4">
						<button class="btn btn-success btn-lg btn-block my-5">Buscar</button>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-section ==========-->

		<!--========== container-mid ==========-->
		<div class="container-mid">
			<div class="container">				
				<div class="row cm-cont py-4">
				    <div class="col-sm-12 col-md-12 col-lg-6">
				        <div class="title-line">																							
							<h1 class="tl-title text-title text-rob tr-1">Encuentra al profesional para tu empresa.</h1>
						</div>
						<div class="row">
							<div class="col-md-3 text-center">
								<i class="icon-users-outline"></i>
							</div>
							<div class="col-md-9">
								<p>
									Los USUARIOS crean su cuenta y describen su perfil de manera que las empresas  sean capaz  de entenderla y así ganar su confianza.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 text-center">
								<i class="icon-commerical-building"></i>
							</div>
							<div class="col-md-9">
								<p>
									Las EMPRESAS PUBLICAN EMPLEOS y si tu perfil coincide con lo que está buscando te llegará un mensaje de confirmación a tu cuenta. PRACTICAMENTE te busca un trabajo.									
								</p>
							</div>
						</div>
				    </div>
				    <div class="col-sm-12 col-md-12 col-lg-6">
				        <img src="images/portada/workred.png">
				    </div>
				</div>
			</div>
		</div>
		<!--========== /container-mid ==========-->

		<!--========== container-mid ==========-->
		<div class="container-mid" style="background-color: #f8f8f8;">
			<div class="container">				
				<div class="row cm-cont py-4">
				    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
				        <div class="title-line">																							
							<h1 class="tl-title text-title text-rob tr-1">Empresas quieren CONTACTARSE con tigo:</h1>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-5 col-lg-3 text-center">
								<i class="icon-mail"></i>
							</div>
							<div class="col-sm-12 col-md-7 col-lg-9">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas quisquam error iure assumenda molestiae. Maxime, ab sed at, neque quis debitis culpa. Itaque libero id aliquam, minus voluptatem quis. Quia.</p>
							</div>
						</div>
				    </div>
				    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
						<img src="images/portada/work.png">										
				    </div>
				</div>
			</div>
		</div>
		<!--========== /container-mid ==========-->

		<!--========== container-mid ==========-->
		<div class="container-mid" style="background:#ffffff;">
			<div class="container">				
				<div class="row cm-cont py-4">
				    <div class="col-sm-12 col-md-12 col-lg-6">
				        <div class="title-line">																							
							<h1 class="tl-title text-title text-rob tr-1">Encuentra al profesional para tu empresa.</h1>
						</div>
						<div class="row">
							<div class="col-md-3 text-center">
								<i class="icon-users-outline"></i>
							</div>
							<div class="col-md-9">
								<p>
									Los USUARIOS crean su cuenta y describen su perfil de manera que las empresas  sean capaz  de entenderla y así ganar su confianza.
								</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 text-center">
								<i class="icon-commerical-building"></i>
							</div>
							<div class="col-md-9">
								<p>
									Las EMPRESAS PUBLICAN EMPLEOS y si tu perfil coincide con lo que está buscando te llegará un mensaje de confirmación a tu cuenta. PRACTICAMENTE te busca un trabajo.									
								</p>
							</div>
						</div>
				    </div>
				    <div class="col-sm-12 col-md-12 col-lg-6">
				        <img src="images/portada/workred.png">
				    </div>
				</div>
			</div>
		</div>
		<!--========== /container-mid ==========-->

		<!--========== container-section ==========-->
		<div class="container-section cs-orange clearfix">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="so-text my-4">Quiero unirme a ésta COMUNIDAD</h3>
					</div>
					<div class="col">
						<div class="d-flex justify-content-end">
							<button class="btn btn-dark-blue b-left mt-4">CLICK AQUÍ</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--========== /container-section ==========-->