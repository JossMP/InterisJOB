		<div class="container-main">
			<div class="container container-input">
				<div class="d-flex justify-content-center">	
					<div class="cont-center" style="width:70%;">
						<h2 class="title-div pb-4"><span>Validacion de Registro</span></h2>
						<?php if($this->msg_error):?>
						<div class="alert alert-danger" role="alert">
							<?= $this->msg_error;?>
						</div>
						<?php endif;?>
						<div class="row">
							<?php if(!$this->request->codeurl):?>
							<div class="col-sm">
								<form class="form-horizaontal" method="POST">
									<div class="form-group row">										
										<div class="col-sm-12 col-md-12 col-lg-12">
											<label for="code" class="col-form-label font-weight-bold">Nombre</label>
										</div>
										<div class="col-sm-12 col-md-12 col-lg-12">
											<input type="text" class="form-control" id="code" name="code" placeholder="Codigo de Validacion" value="<?=$this->request->code;?>" required>
										</div>
									</div>
									<div class="d-flex justify-content-end">							
										<button type="submit" class="btn btn-dark-blue mx-auto">Validar</button>
									</div>
								</form>
							</div>
							<?php else: ?>
							<div class="col-sm">
								<div class="alert alert-<?=$this->msg_validation_type;?>" role="alert">
									<?=$this->msg_validation;?>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
