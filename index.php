<?php
	session_name("interisID");
	session_start();
	
	define( 'APP', __DIR__ . DIRECTORY_SEPARATOR );
	
	// opcional, solo si se tienes dependencias installadas (lado Servidor)
	if ( file_exists(APP . 'vendor/autoload.php') )
	{
		require_once APP . 'vendor/autoload.php';
	}

	require_once APP . 'config.php';
	require_once APP . 'application/bootstrap.php';
