$(function(){

   $(document).on("click","#idProfile",function(){
        $(this).children('i').toggleClass("icon-up-open-1");
    });

    //---

    $(document).on("click",".menu-filter>li>label", function(){
        
        if($(this).children("input:checkbox").is(':checked')){
            $(this).addClass("mf-active");
        }else{
            $(this).removeClass("mf-active");
        }
        
    });
    
    $(document).on('change','#checkTodos',function(){
        
        $(".menu-filter>li>label input:checkbox").prop('checked', $(this).prop("checked"));
        $(".menu-filter>li>label").each(function(){
            if($(this).children("input:checkbox").is(':checked')){
                $(this).addClass("mf-active");
            }else{
                $(this).removeClass("mf-active");
            }
        });
        
    });

    //---

    $(document).on("click",".cs-radio>label",function(){
        $($(this).children("input:radio").attr("present")).addClass('show').siblings().removeClass('show');
        
    });

    //---
    
    $(document).on("click",".menu-bar",function(){
        $(this).toggleClass("show");
    });

    //---

    var elem=$('.container-top');
    var eh=elem.height();
    $(window).scroll(function(){
        if($(window).scrollTop()>eh){
            elem.addClass('change');
        }else{
            elem.removeClass('change');
        }
    });
});
